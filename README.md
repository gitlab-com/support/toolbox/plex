## Manage multiple nodes in the Googleplex (GCP)

Manage multiple "nodes" in the Googleplex (GCP compute instances), for
GitLab Sandbox Cloud deployments.

SYNOPSIS

  `plex [options] [ACTION] [PATTERN]`

- _ACTION_ may be one of the `gcloud compute instances` actions or extended
      actions. If no _ACTION_ is given, then nodes will be commanded to stop.
- _PATTERN_ is a regex to match multiple nodes by their names. If no 
      _PATTERN_ is given, then _ACTION_ commands are sent to a pre-determined list of patterns and zones.

The `plex` script is a wrapper for the `gcloud compute instances` command-line
interface for GCP. It can pass _ACTION_ s to multiple nodes at a time. For more
information about available _ACTION_ s, consult the 'gcloud compute instances' 
sub-command's built-in help.

Note that to contact the 'plex, you must be logged into Google Cloud:

```
$ gcloud auth login
```

## Details

The script is self-documenting. Use the `-h` or `--help` option to see full 
details of the script and extended actions.
## Customizing

- You can override defaults, but you might enjoy the script more by changing them
- See the end of the script for pre-determined _PATTERN_s and GCP zones. This will need customizing to your own needs, if you intend to run without any arguments (for instance, to stop all nodes from a cron job).
